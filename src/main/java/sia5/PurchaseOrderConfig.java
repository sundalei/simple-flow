package sia5;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
public class PurchaseOrderConfig {

    @Bean
    public MessageChannel poChannel() {
        return new DirectChannel();
    }
}
