package sia5;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.file.FileHeaders;
import org.springframework.messaging.handler.annotation.Header;
import sia5.order.PurchaseOrder;

@MessagingGateway(defaultRequestChannel = "poChannel")
public interface PurchaseOrderGateway {

    void writeToFile(
            @Header(FileHeaders.FILENAME) String filename, PurchaseOrder purchaseOrder);
}
