package sia5.splitter;

import sia5.order.PurchaseOrder;

import java.util.ArrayList;
import java.util.Collection;

public class OrderSplitter {

    public Collection<Object> splitOrderIntoParts(PurchaseOrder po) {
        ArrayList<Object> parts = new ArrayList<>();
        parts.add(po.getBillingInfo());
        parts.add(po.getLineItems());
        return parts;
    }
}
