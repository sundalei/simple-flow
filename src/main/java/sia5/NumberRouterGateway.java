package sia5;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.file.FileHeaders;
import org.springframework.messaging.handler.annotation.Header;

@MessagingGateway(defaultRequestChannel = "numberChannel")
public interface NumberRouterGateway {

    void writeToFile(
            @Header(FileHeaders.FILENAME) String filename,
            String number);
}
