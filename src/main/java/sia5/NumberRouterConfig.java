package sia5;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.annotation.Router;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.router.AbstractMessageRouter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

@Configuration
public class NumberRouterConfig {

    @Profile("javaconfig")
    @Bean
    @Router(inputChannel = "numberChannel")
    public AbstractMessageRouter evenOddRouter() {
        return new AbstractMessageRouter() {
            @Override
            protected Collection<MessageChannel> determineTargetChannels(Message<?> message) {
                String numberStr = (String) message.getPayload();
                Integer number = Integer.valueOf(numberStr);
                if (number % 2 == 0) {
                    return Collections.singleton(evenChannel());
                }
                return Collections.singleton(oddChannel());
            }
        };
    }

    @Profile({"javaconfig", "javadsl"})
    @Bean
    public MessageChannel evenChannel() {
        return new DirectChannel();
    }

    @Profile({"javaconfig", "javadsl"})
    @Bean
    public MessageChannel oddChannel() {
        return new DirectChannel();
    }

    @Profile("javaconfig")
    @Bean
    @ServiceActivator(inputChannel = "evenChannel")
    public FileWritingMessageHandler evenFileWriter() {
        FileWritingMessageHandler handler =
                new FileWritingMessageHandler(new File("/Users/sundalei/sia5/files/even"));
        handler.setExpectReply(false);
        handler.setFileExistsMode(FileExistsMode.APPEND);
        handler.setAppendNewLine(true);
        return handler;
    }

    @Profile("javaconfig")
    @Bean
    @ServiceActivator(inputChannel = "oddChannel")
    public FileWritingMessageHandler oddFileWriter() {
        FileWritingMessageHandler handler =
                new FileWritingMessageHandler(new File("/Users/sundalei/sia5/files/odd"));
        handler.setExpectReply(false);
        handler.setFileExistsMode(FileExistsMode.APPEND);
        handler.setAppendNewLine(true);
        return handler;
    }

    @Profile("javadsl")
    @Bean
    public IntegrationFlow numberRoutingFlow() {
        return IntegrationFlows
                .from("numberChannel")
                .<String, String>route(number -> (Integer.valueOf(number)) % 2 == 0 ? "EVEN" : "ODD",
                        mapping -> mapping.subFlowMapping("EVEN",
                                    sf -> sf.handle(Files.outboundAdapter(new File("/Users/sundalei/sia5/files/even"))
                                .fileExistsMode(FileExistsMode.APPEND)
                                .appendNewLine(true)))
                                .subFlowMapping("ODD",
                                        sf -> sf.handle(Files.outboundAdapter(new File("/Users/sundalei/sia5/files/odd"))
                                        .fileExistsMode(FileExistsMode.APPEND)
                                        .appendNewLine(true))))
                .get();
    }
}
