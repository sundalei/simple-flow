package sia5;

import java.io.File;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.core.GenericSelector;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.messaging.MessageChannel;

@Configuration
public class FileWriterIntegrationConfig {
	
	@Configuration
	@Profile("xmlconfig")
	@ImportResource("classpath:/filewriter-config.xml")
	public static class XmlConfiguration {}
	
	@Profile({"javaconfig", "javadsl"})
	@Bean
	public MessageChannel textInChannel() {
		return new PublishSubscribeChannel();
	}
	
	@Profile({"javaconfig", "javadsl"})
	@Bean
	public MessageChannel fileWriterChannel() {
		return new PublishSubscribeChannel();
	}
	
	@Profile("javaconfig")
	@Bean
	@Transformer(inputChannel = "textInChannel", outputChannel = "fileWriterChannel")
	public GenericTransformer<String, String> upperCaseTransformer() {
		return text -> text.toUpperCase();
	}
	
	@Profile("javaconfig")
	@Bean
	@ServiceActivator(inputChannel="fileWriterChannel")
	public FileWritingMessageHandler fileWriter() {
		FileWritingMessageHandler handler = new FileWritingMessageHandler(new File("/tmp/sia5/files"));
		handler.setExpectReply(false);
		handler.setFileExistsMode(FileExistsMode.APPEND);
		handler.setAppendNewLine(true);
		return handler;
	}
	
	@Profile("javadsl")
	@Bean
	public IntegrationFlow fileWriterFlow() {
		return IntegrationFlows
				.from("textInChannel")
				.<String, String>transform(t -> t.toUpperCase())
				.channel("fileWriterChannel")
				.filter(source -> {
					if(source instanceof String) {
						return ((String)source).toLowerCase().contains("hello");
					}
					return false;
				})
				.handle(Files.outboundAdapter(new File("/Users/sundalei/sia5/files"))
						.fileExistsMode(FileExistsMode.APPEND)
						.appendNewLine(true))
				.get();
	}
}
