package sia5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import sia5.order.BillingInfo;
import sia5.order.LineItem;
import sia5.order.PurchaseOrder;

import java.util.ArrayList;

@SpringBootApplication
@Slf4j
public class SimpleFlowApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleFlowApplication.class, args);
    }

    /*
    @Bean
    public CommandLineRunner writeData(FileWriterGateway gateway, Environment env) {
    	return args -> {
    		String[] activeProfiles = env.getActiveProfiles();
    		if(activeProfiles.length > 0) {
    			String profile = activeProfiles[0];
    			gateway.writeToFile("simple.txt", "Hello, Spring Integration! (" + profile + ")");
    		} else {
    			System.out.println("No active profile set. Should set active profile to one of xmlconfig, javaconfig, or javadsl.");
    		}
    	};
    }
    */

    /*
    @Bean
    public CommandLineRunner writeNumber(NumberRouterGateway routerGateway, Environment env) {
    	return args -> {
    		for(int i = 0; i < 500; i++) {
				System.out.println("i = " + i);
				routerGateway.writeToFile("number.txt", String.valueOf(i));
			}
		};
	}
	*/

    @Bean
	public CommandLineRunner writeOrder(PurchaseOrderGateway purchaseOrderGateway, Environment env) {
    	return args -> {
    	    log.info("purchaseOrderGateway type : " + purchaseOrderGateway.getClass().getName());

            BillingInfo billingInfo = new BillingInfo();
            billingInfo.setId("bill1");
            billingInfo.setPrice(100.00);

            LineItem lineItem1 = new LineItem();
            lineItem1.setId("lineItem1");
            lineItem1.setName("mac");

            LineItem lineItem2 = new LineItem();
            lineItem2.setId("lineItem2");
            lineItem2.setName("pc");

            PurchaseOrder purchaseOrder = new PurchaseOrder();
            purchaseOrder.setBillingInfo(billingInfo);

            ArrayList<LineItem> lineItems = new ArrayList<>();
            lineItems.add(lineItem1);
            lineItems.add(lineItem2);

            purchaseOrder.setLineItems(lineItems);

            purchaseOrderGateway.writeToFile("order.txt", purchaseOrder);

		};
	}

}

